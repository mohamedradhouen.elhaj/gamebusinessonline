//
//  NewsService.swift
//  GameBusinessOnline
//
//  Created by Med Radhouen Elhaj on 11/1/18.
//  Copyright © 2018 Esprit. All rights reserved.
//
import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
class NewsService {
    init() {
        
    }
    func  fetchNews (completion: @escaping (Array<News>?)-> Void) {
        let url_service = "https://www.gamesundbusiness.de/index.php?id=581"
        Alamofire.request(url_service).responseArray { (response: DataResponse<[News]>) in
            switch response.result {
            case .success( _):
                if response.result.value != nil{
                    completion(response.result.value!)
                }
            case .failure(let error):
                completion(nil)
                print(error)
            }
            
            
        }
//        Alamofire.request(url_service).responseArray(keyPath: nil) { (response: DataResponse<[News]>) in
//
//            switch response.result {
//            case .success( _):
//
//                if response.result.value != nil{
//                    if let News = response.result.value{
//                        let NewsArray = News
//                        completion(NewsArray)
//                    }else{
//                        print("Null values")
//                        completion(nil)
//                    }
//                }
//            case .failure(let error):
//                print(response)
//                print("----------------------------------------------")
//                completion(nil)
//                print("failed to parse")
//                print(error)
//            }
//        }
    }
}
