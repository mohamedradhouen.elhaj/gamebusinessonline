//
//  ViewController.swift
//  GameBusinessOnline
//
//  Created by Med Radhouen Elhaj on 11/1/18.
//  Copyright © 2018 Esprit. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var EmptyView: UIView!
    var newsList = [News]()
    let newService = NewsService()
    var selectedNews : News?
 
    
    override func viewWillAppear(_ animated: Bool) {
        EmptyView.isHidden = true
        fetchData()
        
       

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if newsList.count>0 {
            tableView.isHidden = false
            EmptyView.isHidden = true
            return newsList.count
        }else{
            tableView.isHidden = true
            EmptyView.isHidden = false
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as! NewsCell
        
        if newsList[indexPath.row].newsTitle != nil {
            cell.titleNews.text = newsList[indexPath.row].newsTitle!
        }else{
            cell.titleNews.text = ""
        }
        if newsList[indexPath.row].newsShortDesc != nil {
            let str = newsList[indexPath.row].newsShortDesc!
            let trimmed = str.trimmingCharacters(in: .whitespacesAndNewlines)
            cell.descriptionNews.text = trimmed
        }else{
            cell.descriptionNews.text = ""
        }
        if newsList[indexPath.row].newsThumbImage != nil {
            let imageurl = "https://www.gamesundbusiness.de\(newsList[indexPath.row].newsThumbImage!)"
            let downloadURL = NSURL(string: imageurl)
            cell.imageNews.af_setImage(withURL: downloadURL! as URL)
        }else{
            
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(newsList[indexPath.row].newsId!)
        selectedNews = newsList[indexPath.row]
        performSegue(withIdentifier: "toDetails", sender: nil)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.75){
            cell.alpha = 1.0
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetails"{
            if let destinationVC = segue.destination as? DetailViewController {
                destinationVC.News = selectedNews
            }
        }
    }
    func fetchData() {
       self.ShowLoader(message: "Vorbereitung der Daten ...")
        if Reachability.isConnectedToNetwork() {
            newService.fetchNews(){ (data) in
                
                if (data != nil){
                    self.newsList.removeAll()
                    self.newsList = data!
                    self.tableview.reloadData()
                    self.hideLoaderWithoutMessage()
                }else{
                    self.hideLoaderWithoutMessage()
                }
                
            }
        }else{
            self.hideLoaderWithoutMessage()
            let alert = UIAlertController(title: "Kein Internetzugang", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aktualisierung", style: .default, handler: { action in
                self.fetchData()
            }))
            
            self.present(alert, animated: true)
            
        }
        
    }

}

