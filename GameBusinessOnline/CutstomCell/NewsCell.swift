//
//  NewsCell.swift
//  GameBusinessOnline
//
//  Created by Med Radhouen Elhaj on 11/3/18.
//  Copyright © 2018 Esprit. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    

    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var titleNews: UILabel!
    @IBOutlet weak var descriptionNews: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

