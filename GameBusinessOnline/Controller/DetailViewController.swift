//
//  DetailViewController.swift
//  GameBusinessOnline
//
//  Created by Med Radhouen Elhaj on 11/3/18.
//  Copyright © 2018 Esprit. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class DetailViewController: UIViewController {
    
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var newDescription: UITextView!
    var News : News?
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }
    
    func prepareView() {
        if News != nil {
            self.navigationItem.title = News?.newsSubTitle!
            newDescription.text = News?.newsBody!.htmlToString
            let url = News?.newsOriImage!
            let imageurl = "https://www.gamesundbusiness.de\(url!)"
            let downloadURL = NSURL(string: imageurl)
            imageNews.af_setImage(withURL: downloadURL! as URL)
            
        }
        
    }
    

}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
