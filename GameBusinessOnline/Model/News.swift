//
//  News.swift
//  GameBusinessOnline
//
//  Created by Med Radhouen Elhaj on 11/1/18.
//  Copyright © 2018 Esprit. All rights reserved.
//

import Foundation
import ObjectMapper

class News : Mappable {
    var newsId : Int?
    var newsTitle : String?
    var newsSubTitle : String?
    var newsOriImage : String?
    var newsThumbImage : String?
    var newsShortDesc : String?
    var newsBody : String?
    var newsCat : String?
    var newsUrl : String?
    
    struct  NewsKeys {
        static let newsId  = "newsId"
        static let newsTitle = "newsTitle"
        static let newsSubTitle = "newsSubTitle"
        static let newsOriImage = "newsOriImage"
        static let newsThumbImage = "newsThumbImage"
        static let newsShortDesc = "newsShortDesc"
        static let newsBody = "newsBody"
        static let newsCat = "newsCat"
        static let newsUrl = "newsUrl"
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        newsId    <- map[NewsKeys.newsId]
        newsTitle <- map[NewsKeys.newsTitle]
        newsSubTitle <- map[NewsKeys.newsSubTitle]
        newsOriImage <- map[NewsKeys.newsOriImage]
        newsThumbImage <- map[NewsKeys.newsThumbImage]
        newsShortDesc <- map[NewsKeys.newsShortDesc]
        newsBody <- map[NewsKeys.newsBody]
        newsCat <- map[NewsKeys.newsCat]
        newsUrl <- map[NewsKeys.newsUrl]
    }
}
