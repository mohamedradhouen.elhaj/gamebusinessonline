//
//  ViewControllerExtension.swift
//  Esprit Etudiant
//
//  Created by Med Radhouen Elhaj on 4/29/18.
//  Copyright © 2018 Esprit. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD

extension UIViewController {

    
    func ShowLoader(message : String) {
        RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: RappleAppleAttributes)
    }
    
    func hideLoader(message : String ,indicator : Int)  {
        switch indicator {
        case 0 :
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .success, completionLabel: message, completionTimeout: 0.5)
            break
        case 1 :
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .failed, completionLabel: message, completionTimeout: 0.5)
            break
        case 2 :
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: message, completionTimeout: 0.5)
            break
        case 3 :
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .unknown, completionLabel: message, completionTimeout: 0.0)
            break
        case 4 :
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .incomplete, completionLabel: message, completionTimeout: 0.5)
            break
        default :
            break
        }
    }
    func hideLoaderWithoutMessage()  {
        RappleActivityIndicatorView.stopAnimation()
    }
  
}
